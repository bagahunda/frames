import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import frame from '@/components/frame'
import preview from '@/components/preview'
import finalPreview from '@/components/finalPreview'
// import admin from '@/components/admin'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/lijstenmaker',
      name: 'home',
      component: home,
      props: true
    },
    {
      path: '/lijstenmaker/frame',
      name: 'frame',
      component: frame,
      props: true
    },
    {
      path: '/lijstenmaker/preview',
      name: 'preview',
      component: preview,
      props: true
    },
    {
      path: '/lijstenmaker/preview/:price',
      name: 'finalPreview',
      component: finalPreview
    }
    // {
    //   path: '/admin',
    //   name: 'admin',
    //   component: admin
    // }
  ]
})

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import ToggleButton from 'vue-js-toggle-button'
import store from './store'
// import idb from 'idb'

Vue.use(ToggleButton)

Vue.config.productionTip = false

/* eslint-disable no-new */
const root = new Vue({
  store,
  router,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyCP6zefMgze5yR_Ak5krfahcUQeJ3H9vOM',
      authDomain: 'framer-app-f606c.firebaseapp.com',
      databaseURL: 'https://framer-app-f606c.firebaseio.com',
      projectId: 'framer-app-f606c',
      storageBucket: 'gs://framer-app-f606c.appspot.com'
    })

    let local = localStorage.getItem('frame')

    if (local) {
      this.$store.dispatch('setState', JSON.parse(local))
      this.$router.push('/lijstenmaker/frame')
    }
  }
})

document.addEventListener('DOMContentLoaded', () => {
  root.$mount('#app')
})

import * as firebase from 'firebase'

export const setState = (state, payload) => {
  state.previewOptions = payload
}

export const setStartSize = (state, payload) => {
  state.previewOptions.width = payload.width * 1
  state.previewOptions.height = payload.height * 1
  localStorage.setItem('frame', JSON.stringify(state.previewOptions))
}

export const setPreviewContainer = (state, payload) => {
  state.previewContainer = payload
}

export const setNewSize = (state) => {
  let borderScale = 4 / state.previewOptions.width
  let paddingsScale = 9 / state.previewOptions.width
  let correct = (state.previewContainer - 40) / (borderScale * 2 + paddingsScale * 2 + 1)
  state.previewOptions.baseWidth = Math.round(correct)
  state.previewOptions.customHeight = Math.round((state.previewOptions.baseWidth) * (state.previewOptions.height / state.previewOptions.width))
}

export const setStartPrice = (state, payload) => {
  if (payload === 20) {
    state.previewOptions.startPrice = 50
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload > 20 && payload <= 30) {
    state.previewOptions.startPrice = 65
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload > 30 && payload <= 40) {
    state.previewOptions.startPrice = 85
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload > 40 && payload <= 50) {
    state.previewOptions.startPrice = 105
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload > 50 && payload <= 60) {
    state.previewOptions.startPrice = 140
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload > 60 && payload <= 75) {
    state.previewOptions.startPrice = 200
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload > 75 && payload <= 90) {
    state.previewOptions.startPrice = 270
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload > 90 && payload <= 100) {
    state.previewOptions.startPrice = 370
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  }
}

export const setFrameWidthChanged = (state, payload) => {
  if (payload === '2') {
    state.previewOptions.framePrice = 0
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  }
  if (payload === '3') {
    state.previewOptions.framePrice = 4
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  }
  if (payload === '4') {
    state.previewOptions.framePrice = 7
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  }
  let scale = (state.previewOptions.frameWidth) / state.previewOptions.width
  state.previewOptions.borderWidth = Math.round(state.previewOptions.baseWidth * scale)
  localStorage.setItem('frame', JSON.stringify(state.previewOptions))
}

export const setFrameColor = (state, payload) => {
  state.previewOptions.frameClass = payload
  localStorage.setItem('frame', JSON.stringify(state.previewOptions))
}

export const setMatWidthChanged = (state, payload) => {
  state.previewOptions.matWidth = payload
  let scale = (state.previewOptions.matWidth) / state.previewOptions.width
  state.previewOptions.paddings = Math.round(state.previewOptions.baseWidth * scale)
  localStorage.setItem('frame', JSON.stringify(state.previewOptions))
}

export const setMatShowed = (state) => {
  if (state.previewOptions.useMat === true) {
    state.previewOptions.matWidth = '3'
    let scale = (state.previewOptions.matWidth) / state.previewOptions.width
    state.previewOptions.paddings = Math.round(state.previewOptions.baseWidth * scale)
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else {
    state.previewOptions.paddings = '0'
    state.previewOptions.matWidth = '0'
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  }
}

export const setGlazingChanged = (state, payload) => {
  if (payload === '20') {
    state.previewOptions.uvPrice = state.previewOptions.startPrice / 10
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload === '30') {
    state.previewOptions.uvPrice = state.previewOptions.startPrice / 4
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  } else if (payload === '10') {
    state.previewOptions.uvPrice = 0
    localStorage.setItem('frame', JSON.stringify(state.previewOptions))
  }
}

export const setLoader = (state) => {
  state.previewOptions.loadPreviewImage = true
}

export const setImgFile = (state, payload) => {
  const files = payload
  const fileReader = new FileReader()
  fileReader.addEventListener('load', () => {
    state.previewOptions.bg = 'url(' + fileReader.result + ')'
  })
  fileReader.readAsDataURL(files[0])
  state.previewOptions.image = files[0]
}

export const uploadImg = (state) => {
  firebase.storage().ref('framer-app/' + new Date().getTime() + state.previewOptions.image.name).put(state.previewOptions.image)
    .then(fileData => {
      state.previewOptions.imageUrl = fileData.metadata.downloadURLs
      state.previewOptions.image = null
      state.previewOptions.bg = ''
      state.previewOptions.loadPreviewImage = false
      localStorage.setItem('frame', JSON.stringify(state.previewOptions))
      firebase.database().ref('framer-app').push({url: fileData.metadata.downloadURLs, name: fileData.metadata.name})
    })
}

export const setError = (state, payload) => {
  state.errorMessage = payload
  state.showError = true
}

export const clearError = (state) => {
  state.errorMessage = ''
  state.showError = false
}

export const setUploadedImg = (state, payload) => {
  state.previewOptions.imageUrl = payload
}

export const setFirebaseImages = (state, payload) => {
  state.firebaseImages = payload
}

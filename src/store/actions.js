import * as firebase from 'firebase'

export const setState = ({commit}, payload) => {
  commit('setState', payload)
}

export const setStartSize = ({commit}, payload) => {
  commit('setStartSize', payload)
}

export const setPreviewContainer = ({commit}, payload) => {
  commit('setPreviewContainer', payload)
}

export const setNewSize = ({commit}) => {
  commit('setNewSize')
}

export const setStartPrice = ({commit}, payload) => {
  commit('setStartPrice', payload)
}

export const setFrameColor = ({commit}, payload) => {
  commit('setFrameColor', payload)
}

export const setFrameWidthChanged = ({commit}, payload) => {
  commit('setFrameWidthChanged', payload)
}

export const setMatWidthChanged = ({commit}, payload) => {
  commit('setMatWidthChanged', payload)
}

export const setMatShowed = ({commit}) => {
  commit('setMatShowed')
}

export const setGlazingChanged = ({commit}, payload) => {
  commit('setGlazingChanged', payload)
}

export const setBg = ({commit}, payload) => {
  commit('setLoader')
  commit('setImgFile', payload)
  commit('uploadImg')
}

export const setUploadedImg = ({commit}, payload) => {
  commit('setUploadedImg', payload)
}

export const setError = ({commit}, payload) => {
  commit('setError', payload)
  setTimeout(() => {
    commit('clearError')
  }, 2000)
}

export const setFirebaseImages = ({commit}) => {
  firebase.database().ref('framer-app').once('value')
    .then(snapshot => {
      const images = []
      const obj = snapshot.val()
      for (let key in obj) {
        images.push({id: key, url: obj[key].url, name: obj[key].name})
      }
      commit('setFirebaseImages', images)
    })
}

export const deleteImage = ({commit}, payload) => {
  let target = firebase.storage().ref().child('framer-app/' + payload.name)
  target.delete()
    .then(() => {
      firebase.database().ref('framer-app').child(payload.id).remove()
    })
    .catch(err => {
      console.log(err)
    })
}

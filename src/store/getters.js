export const getPreviewOptions = (state) => {
  return state.previewOptions
}

export const getTotalPrice = (state) => {
  return state.previewOptions.startPrice * 1 + state.previewOptions.matPrice * 1 + state.previewOptions.uvPrice * 1
}

export const hasError = (state) => {
  return state.showError
}

export const getError = (state) => {
  return state.errorMessage
}

export const getFirebaseImages = (state) => {
  return state.firebaseImages
}
